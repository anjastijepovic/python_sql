'''
1. Insert at least 5 inventory items through python code
2. Insert records into the orders table with various order dates and quantities of items.
3. Write Python function which returns all data from inventory table except item_id
4. Write Python function  which returns all orders from orders table for given inventory item.
5. Write a function which checks if for a given order there in enough items in the inventory
(only this order needs to be checked, not the rest of the orders for the same item)
'''

from db_connection import cursor, db

#1
def add_inventory_item(item_name, price, quantity):
    sql = ("INSERT INTO inventory(item_name, price, quantity) VALUES (%s, %s, %s)")
    cursor.execute(sql, (item_name, price, quantity,))
    db.commit()
    item_id = cursor.lastrowid
    print("Added inventory item {}".format(item_id))

#2
def add_order(item_id, quantity_ordered, order_date):
    sql = ("INSERT INTO orders(item_id, quantity_ordered, order_date) VALUES (%s, %s, %s)")
    cursor.execute(sql, (item_id, quantity_ordered, order_date,))
    db.commit()
    order_id = cursor.lastrowid
    print("Added order item {}".format(order_id))

#3
def get_inventory():
    sql = ("SELECT item_name, price, quantity FROM inventory")
    cursor.execute(sql)
    result = cursor.fetchall()

    for row in result:
        print(row)

#4
def get_order(item_id):
    sql = ("SELECT * FROM orders WHERE item_id = %s ORDER BY order_date DESC") 
    cursor.execute(sql, (item_id,))
    result = cursor.fetchall()

    for row in result:
        print(row)

#5
def check_order(order_id):
    sql1 = ("SELECT item_id, quantity_ordered FROM orders WHERE order_id = %s")
    cursor.execute(sql1, (order_id,))
    order_details = cursor.fetchone() #order_details je (item_id, quantity_ordered)
    item_id = order_details[0] #da bismo sačuvali item_id

    sql2 = ("SELECT quantity FROM inventory WHERE item_id = %s")
    cursor.execute(sql2, (item_id,))
    inventory_quantity = cursor.fetchone() #(quantity)

    if order_details[1] > inventory_quantity[0]: #ako je quantity_ordered > quantity
        return False
    return True


# add_inventory_item('Pen', 2, 200)
# add_inventory_item('Pencil', 1, 500)
# add_inventory_item('Notebook', 2.5, 5000)
# add_inventory_item('Rubber', 0.5, 500)
# add_inventory_item('Glitter', 3, 200)
        
# get_inventory()

# add_order(1, 200, '2024-02-01')
# add_order(1, 50, '2024-01-15')
# add_order(2, 50, '2024-01-30')
# add_order(3, 100, '2024-01-10')
# add_order(4, 5, '2024-01-05')
#add_order(1, 500, '2024-02-01')

# get_order(1)
    

#print(check_order(6))
#print(check_order(1))
