'''
1. Write Python code to update the quantity of a specific inventory item in the inventory table.

2. Update the quantity of an ordered item in the orders table.

'''

from db_connection import cursor, db
#from assignment_2 import get_inventory

def update_quantity_inventory(item_id, new_quantity):
    sql = ("UPDATE inventory SET quantity = %s WHERE item_id = %s")
    cursor.execute(sql, (new_quantity, item_id))
    db.commit()
    print("Item quantity in the inventory updated")


def update_quantity_ordered(order_id, new_quantity):
    sql = ("UPDATE orders SET quantity_ordered = %s WHERE order_id = %s")
    cursor.execute(sql, (new_quantity, order_id))
    db.commit()
    print("Ordered quantity updated")


update_quantity_inventory(1, 3)
#get_inventory()

update_quantity_ordered(2, 5)

    