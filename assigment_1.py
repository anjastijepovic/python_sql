'''
Create MySQL database called store_management, and within it create 2 tables called inventory and orders.

Inventory shall have the following fields: item_id, item_name, price, quantity
and orders shall have the following fields: order_id, item_id, quantity_ordered, order_date the rest of the orders for the same item

Table ids should be primary keys and auto incremented,
Two tables are connected with a foreign key
Order date should be a date field that is populated by default to the current date
Quantity and Quantity ordered should not be null
'''


#creating database and tables

import mysql.connector
from mysql.connector import errorcode
from db_connection import cursor


DB_NAME = 'store_management'

TABLES = {}

TABLES['inventory'] = (
    "CREATE TABLE `inventory` ("
    " `item_id` int(11) NOT NULL AUTO_INCREMENT,"
    " `item_name` varchar(50) NOT NULL,"
    " `price` int(10) NOT NULL,"
    " `quantity` int(10) NOT NULL,"
    " PRIMARY KEY (`item_id`)"
    ") ENGINE=InnoDB"
)

TABLES['orders'] = (
    "CREATE TABLE `orders` ("
    " `order_id` int(11) NOT NULL AUTO_INCREMENT,"
    " `item_id` int(11) NOT NULL,"
    " `quantity_ordered` int(10) NOT NULL,"
    " `order_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,"
    " PRIMARY KEY (`order_id`),"
    " FOREIGN KEY (`item_id`) REFERENCES `inventory`(`item_id`) ON DELETE CASCADE ON UPDATE CASCADE"
    #kad izbrišemo item iz inventory, orders sa istim item_id će biti obrisane (assignment 19) I isto za update
    ") ENGINE=InnoDB"
)

def create_database():
    cursor.execute("CREATE DATABASE IF NOT EXISTS {} DEFAULT CHARACTER SET 'utf8'".format(DB_NAME))
    print("Database {} created!".format(DB_NAME))


def create_tables():
    cursor.execute("USE {}".format(DB_NAME))

    for table_name in TABLES:
        table_description = TABLES[table_name]
        try:
            print("Creating table ({}) ".format(table_name), end="")
            cursor.execute(table_description)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("Already exists")
            else:
                print(err.msg)


create_database()
create_tables()
