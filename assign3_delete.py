'''
4. Write Python code to delete an inventory item record from the inventory table.

5. Remove an order and its details from the orders table.
'''

from db_connection import cursor, db
#from assignment_2 import get_inventory

def delete_inventory_item(item_id):
    sql = ("DELETE FROM inventory WHERE item_id = %s")
    cursor.execute(sql, (item_id,))
    db.commit()
    print("Item and orders of item removed")

def delete_order(order_id):
    sql = ("DELETE FROM orders WHERE order_id = %s")
    cursor.execute(sql, (order_id,))
    db.commit()
    print("Order removed")

#delete_inventory_item(2)
#get_inventory()

